#!/usr/bin/env python
# coding: utf-8
import numpy as np
import pandas as pd
import re
import time
import pickle
import mmh3
from multiprocessing import Pool, cpu_count, Process
from functools import partial
from lshcore import MinHash, MinHashLSH, LeanMinHash
from tqdm import tqdm
import gc
import sys

cpu_count = cpu_count()

def sizeof_fmt(num, suffix='B'):
    ''' by Fred Cirera,  https://stackoverflow.com/a/1094933/1870254, modified'''
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f %s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f %s%s" % (num, 'Yi', suffix)

def ngrams(text, n):
    words = [text[i:i + n] for i in range(0, len(text) - (n - 1), 1)]
    return words

def preprocess(row):
    list_tokens = []
    for field in row:
        if type(field) is float:
            continue
        text = re.sub(r'[^\w\s]', '', field)
        tokens = text.lower()
        list_tokens += ngrams(tokens, 2)
    return list_tokens

def _hash_func(d):
    return mmh3.hash(d, signed=False)

def minhash_one_row(index, a, b, d, perms):
    tokens = preprocess([a, b, d])
    m = MinHash(num_perm=perms, hashfunc=_hash_func, ID=index)
    for s in tokens:
        m.update(s.encode('utf8'))
    return m

def create_minhash(perms, data_block):
    arr_block = np.vectorize(minhash_one_row)(data_block.index, data_block['FULL_NAME'],data_block['BIRTHDAY'],data_block['KS'], perms)
    return arr_block

def multi_process_minhash(data, perms):
    print('Number of CPUs: ' + str(cpu_count))
    df_split = np.array_split(data, cpu_count, axis=0)
    pool = Pool(cpu_count)
    func = partial(create_minhash, perms)
    result = pool.map(func, df_split)
    pool.close()
    pool.join()
    gc.collect()
    all_minhash = np.concatenate(result)
    return all_minhash

def write_to_pkl():
    #global time_build_lsh
    global lsh
    start_time= time.time()
    with open("lsh_test9.pkl", "wb") as f:
        pickle.dump(lsh, f)
    timing_lsh = time.time() - start_time
    print('It took %s seconds to inserst LSH into pkl.' % timing_lsh)
    #time_build_lsh.append(timing_lsh)

def working_withLSH(minhash,last_index):
    # if add_new==False:
    #
    # else:
    #     with open('lsh_test8.pkl', 'rb') as f:
    #         start = time.time()
    #         lsh = pickle.load(f)
    #         print('Done loading from pkl:', time.time() - start)
    global lsh
    time.sleep(10)
    start_time = time.time()
    with lsh.insertion_session() as session:
        #time_hash.append(timing)
        with tqdm(total=minhash.size) as pbar:
            for idx, m in enumerate(minhash):
                session.insert(idx + last_index, m)
                pbar.update(1)
    timing = time.time() - start_time

    print('It took %s seconds to insert into LSH' % timing)
    # write_to_pkl()
    # minhash = None
    # del minhash


def index_minhash_threshold(data, perms, threshold, last_index):
    #global time_hash
    start_time = time.time()
    for name, size in sorted(((name, sys.getsizeof(value)) for name, value in locals().items()), key=lambda x: -x[1])[:10]:
        print("{:>30}: {:>8}".format(name, sizeof_fmt(size)))

    minhash = multi_process_minhash(data, perms)

    for name, size in sorted(((name, sys.getsizeof(value)) for name, value in locals().items()), key=lambda x: -x[1])[:10]:
        print("{:>30}: {:>8}".format(name, sizeof_fmt(size)))

    timing = time.time() - start_time
    # time.sleep(10)
    print('It took %s seconds to hash.' % timing)
    working_withLSH(minhash,last_index)


def predict(record, database, perms, lsh, num_results=0):
    '''
    num_results = 0: MinhashLSH
    num_results # 0: MinhashForest
    '''
    global time_query
    start_time = time.time()

    tokens = preprocess(record)
    m = MinHash(num_perm=perms, hashfunc=_hash_func)
    for s in tokens:
        m.update(s.encode('utf8'))

    if num_results == 0:
        idx_array = np.array(lsh.query(m))
    else:
        idx_array = np.array(lsh.query(m, num_results))
    if len(idx_array) == 0:
        return None  # if your query is empty, return none

    result = database.iloc[idx_array]

    print('It took %s seconds to query forest.' % (time.time() - start_time))
    time_query.append((time.time() - start_time))
    return result

# def clean_field_data_csv(path):
#     # Read CSV data
#     db = pd.read_csv(path)
#     db = db.iloc[2:]
#     # del db['LABEL']
#     db.head()
#     db['NGAY'] = db['NGAY'].astype('str').apply(lambda x: "0" + x if len(x) == 1 else x)
#     db['THANG'] = db['THANG'].astype('str').apply(lambda x: "0" + x if len(x) == 1 else x)
#     db['FULL_NAME'] = db['HO'] + ' ' + db['DEM'] + ' ' + db['TEN']
#     db['BIRTHDAY'] = db['NGAY'] + db['THANG'] + db['NAM'].astype('str')
#     db['KS'] = db['MA_TINH_KS'] + ' ' + db['MA_HUYEN_KS'] + ' ' + db['MA_XA_KS'].astype('str')
#     db['IDENTIFY'] = db['SO_CMTND'].astype('str').apply(lambda x: "" if x.find("-") != -1 else x)
#     data = db[['FULL_NAME', 'BIRTHDAY', 'KS', 'IDENTIFY']].copy()
#     data.to_csv('test_full.csv', index=False)
#     data.head()


# def read_cleaned_dataframe(path, dataset):
#     # clean_field_data_csv(path)
#     # Read cleaned CSV
#     db = pd.read_csv(path)
#     db = db[:dataset]
#     print(len(db))
#     db['IDENTIFY'] = db['IDENTIFY'].astype('str')
#     db['BIRTHDAY'] = db['BIRTHDAY'].astype('str')
#     db['IDENTIFY'] = db['IDENTIFY'].astype('str')
#     return db

# Query
def query():
    data = {'FULL_NAME': "Trần Văn Minh",
            'BIRTHDAY': "10011916",
            'KS': "52TTT 053HH 18800",
            'IDENTIFY': "52216886179"}

    df = pd.DataFrame(data, index=[0])
    result = predict(df.loc[0], db, permutations, lsh=lsh)

    print('\n Top Recommendation(s) is(are) \n', result)

# def main():
#     # Read dataframe
#     # clean_field_data_csv('data/new_record.csv')
#     #time_hash, time_build_lsh, time_query = [], [], []
#     # arr = [100, 200, 500, 800, 1000, 2000, 5000, 10000, 15000, 20000]
#     # Number of Permutations
#     permutations = 128
#     # Number recommendations
#     num_recommendations = 32
#     # Threshold
#     threshold = 0.55
#     i = 0
#     j = 0
#     start_build = time.time()
#     start_time = time.strftime('%X %x')
#     print(start_time)
#     data = pd.read_csv('../dataBHXH/test_full_120.csv',dtype='str',chunksize=2000000)
#     for db in data:
#         print ('First loop started at',start_time)
#         print("START")
#         start = time.time()
#         for name, size in sorted(((name, sys.getsizeof(value)) for name, value in locals().items()),key=lambda x: -x[1])[:10]:
#             print("{:>30}: {:>8}".format(name, sizeof_fmt(size)))
#         print (i)
#         time.sleep(30)
#         if i ==0:
#             index_minhash_threshold(db, threshold=threshold, perms=permutations,add_new=False,last_index=j)
#         else:
#             index_minhash_threshold(db, threshold=threshold, perms=permutations,add_new=True,last_index=j)
#         j+=db.shape[0]
#         # print("STOP")
#         # gc.collect()
#         # for name, size in sorted(((name, sys.getsizeof(value)) for name, value in locals().items()),key=lambda x: -x[1])[:10]:
#         #     print("{:>30}: {:>8}".format(name, sizeof_fmt(size)))
#         # p = Process(target=release_memory)
#         # p.start()
#         # p.join()
#         print ('End chunk',i,'in',time.time()-start)
#         i+=1
#
#     print ('Finish building in',time.time()-start_build,'at',time.strftime('%X %x'))

permutations = 128
num_recommendations = 32
threshold = 0.55
lsh = MinHashLSH(threshold=threshold, num_perm=permutations)

if __name__ == '__main__':
    # Read dataframe
    # clean_field_data_csv('data/new_record.csv')
    # time_hash, time_build_lsh, time_query = [], [], []
    # arr = [100, 200, 500, 800, 1000, 2000, 5000, 10000, 15000, 20000]
    # Number of Permutations

    i = 0
    j = 0
    start_build = time.time()
    start_time = time.strftime('%X %x')
    print(start_time)
    data = pd.read_csv('./data/test_full_120.csv' ,dtype='str', chunksize=1000000,nrows=3000000,low_memory=True)
    for db in data:
        print('First loop started at', start_time)
        print("START")
        start = time.time()
        print(i)
        print(j)
        index_minhash_threshold(db, threshold=threshold, perms=permutations, last_index=j)
        # objgraph.show_growth()
        # objgraph.get_leaking_objects()
        j += db.shape[0]
        print('End chunk', i, 'in', time.time() - start)
        print ('')
        i += 1
    write_to_pkl()
    query()

    print('Finish building in', time.time() - start_build, 'at', time.strftime('%X %x'))

