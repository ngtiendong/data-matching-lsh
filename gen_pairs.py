import os
import pickle
import glob
from tqdm import tqdm
import time
import numpy as np
from collections import defaultdict
from itertools import chain


def combine_dict(dict1, dict2):
    res = defaultdict(set)
    for key, val in chain(dict1.items(), dict2.items()):
        res[key].update(val)
    return res


def build_hashtable():
    hashtables = glob.glob("hashtables/*")
    for hashtable in hashtables:
        start_combine_dict = time.time()
        print(hashtable)
        iterations = glob.glob(f'{hashtable}/*.pkl')
        with open(iterations[0], 'rb') as f:
            hashtable_dict = pickle.load(f)
            hashtable_dict = hashtable_dict._dict
        for iteration in iterations[1:]:
            with open(iteration, 'rb') as f:
                temp_dict = pickle.load(f)
                temp_dict = temp_dict._dict
            new_dict = combine_dict(hashtable_dict, temp_dict)
            del temp_dict
            del hashtable_dict
            hashtable_dict = new_dict
            del new_dict
        print("combine iterations in", time.time() - start_combine_dict)
        start_convert_type_at = time.time()
        counter = 0
        for key, val in hashtable_dict.items():
            hashtable_dict[key] = np.array(list(val))
            counter += 1

        print("Cast to array in", time.time() - start_convert_type_at)
        print("hashtable size", counter)
        start_dump_at = time.time()
        with open(f'{hashtable}.pkl', 'wb') as f:
            pickle.dump(hashtable_dict, f)
        print("Dumping hashtable to pkl in", time.time()-start_dump_at)
        del hashtable_dict


def gen_dict_pairs():
    num_dict = 160
    remainder = 1

    result = np.zeros(int(121e6)//num_dict).astype('object')
    start = time.time()
    hashtables = glob.glob("hashtables/*.pkl")
    for hashtable in hashtables:
        print(hashtable)
        with open(hashtable, 'rb') as f:
            table = pickle.load(f)

        for key, arr in tqdm(table.items()):
            length_arr = len(arr)
            table[key] = None
            if length_arr > 1:
                arr = sorted(arr)
                arr = np.array(arr)
                indicies = np.argwhere(arr % num_dict == remainder).T[0]
                for idx in indicies:
                    k = arr[idx]
                    temp_val = result[k//num_dict]
                    if type(temp_val) != np.ndarray:
                        temp_val = np.array([])
                    temp_val = np.append(temp_val, (arr[idx:] - k)/k)
                    temp_val = np.unique(temp_val)
                    result[k//num_dict] = temp_val
        del table
        print("Complete", hashtable, 'in', time.time()-start)

    print("DONE")


def main():
    build_hashtable()
    # gen_dict_pairs()


if __name__ == '__main__':
    main()
