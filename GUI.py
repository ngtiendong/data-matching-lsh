# tkinter la lib tao giao dien GUI (graphic user interface)
from tkinter import *
from tkinter import filedialog,ttk
import final2
# Su dung cac thong bao trong tkinter
import tkinter.messagebox as tmb
from functools import partial
import threading
import psutil
import re
import multiprocessing
import time

#Variable in Build LSH
desti_folder_build_lsh,field,uri_db_build_lsh = '','',''
#Variable in Shrink Data
desti_folder_shrink_data,bucket_file,model_confi,uri_db_shrink_data='','','',''
executing =False

# Ham xu ly dong window
def close_app():
    if button_stop_build['state'] == 'normal':
        tmb.showwarning('A process is still running','Please stop running processes by pressing Stop button')
    else:
        if tmb.askokcancel("Close", "Are you sure...?"):
            window.destroy()

# Ham xu ly convert cac string input thanh cung 1 dang
def clean_path(path):
    if '\\' in path:
        path = path.replace('\\','/')
    return path

# Browse folder
def browse_button_build_LSH():
    global desti_folder_build_lsh
    folder_path= filedialog.askdirectory()
    desti_folder_build_lsh.set(folder_path)

# Ham xu ly chinh cua cac tab- Execute Build LSH
def exeBut_buildLSH():
    global desti_folder_build_lsh,uri_db_build_lsh,field,button_browse,button_execute,button_stop
    # Clean du lieu
    input_path= clean_path(uri_db_build_lsh.get())
    output_path = clean_path(desti_folder_build_lsh.get())+'/'
    list_field = field.get().split(',')
    # Neu nhu cac input ma khong empty thi chay
    if desti_folder_build_lsh.get() != '' and uri_db_build_lsh.get() != '' and field.get() != '' :
        print('Working ...')
        try:
            # Running the main function
            # loading()
            if button_stop_build['state'] == 'disabled':
                # Bien doi lai cac status cua nut de chan nguoi dung chay nhieu lan
                button_browse_build_lsh['state'] = 'disabled'
                button_execute['state'] = 'disabled'
                button_stop_build['state'] = 'normal'

                # Vi khong biet khi nao chay xoong progress bar se chay lap lai
                progress.start()

                start_build = time.time()
                start_time = time.strftime('%X %x')
                # Phai de thang main cua scirpt khac trong Thread vi neu ko de thi chuong trinh se bi 'NOt Respondning Error'
                t = threading.Thread(target=partial(final2.main, input_path, output_path, list_field))# To solve "Not Responding' Error
                t.start()
                global executing
                executing = TRUE
                # tmb.showinfo('Complete', 'It took {} seconds from {} to {}'.format(time.time() - start_build,start_time, time.strftime('%X %x')) )
        except Exception as ex:
            tmb.showerror('Error',ex)
            # print (ex)
    elif desti_folder_build_lsh.get() == '':
        tmb.showwarning("Error", "Destination folder entry is empty")
    elif uri_db_build_lsh.get() == '':
        tmb.showwarning("Error", "URI DB entry is empty")
    elif field.get() == '' :
        tmb.showwarning("Error", "Field entry is empty")

def stop_executing():
    # To kill all related processes, especially when running with multiprocessing
    if executing:
        for p in sorted(psutil.process_iter(attrs=['name', 'memory_percent']), key=lambda p: p.info['memory_percent']):
            if re.search('python.exe',p.info['name']) or re.search('BHXH.exe',p.info['name']):
                p.kill()
    window.quit()
    window.destroy()
    sys.exit()

def build_LSH(tab1):
    global desti_folder_build_lsh,field,uri_db_build_lsh,build_state

    # Tab Build LSH
    # Tao label
    label_uri_db = Label(tab1,text='URI DB').pack()
    # Tao bien de thay doi trong entry
    uri_db_build_lsh = StringVar()
    #  Tao khung chua entry
    entry_box = Entry(tab1,textvariable=uri_db_build_lsh).pack()

    lsh_field = Label(tab1,text='LSH field (seperated by comma)').pack()
    field = StringVar()
    entry_box = Entry(tab1,textvariable=field).pack()

    theLabel = Label(tab1, text='Choose the destination folder').pack()
    desti_folder_build_lsh = StringVar()
    entry_box = Entry(tab1, textvariable=desti_folder_build_lsh).pack()

def browse_button_shrink_data(action):
    if action =='bucket':
        global bucket_file
        filename = filedialog.askopenfilename()
        bucket_file.set(filename)
    elif action =='configuration':
        global model_confi
        filename = filedialog.askopenfilename()
        model_confi.set(filename)
    elif action =='destination':
        global desti_folder_shrink_data
        folder_path = filedialog.askdirectory()
        desti_folder_shrink_data.set(folder_path)

def exeBut_ShrinkData():
    global desti_folder_shrink_data,bucket_file,model_confi,uri_db_shrink_data
    path_value_desti= clean_path(desti_folder_shrink_data.get())+'/'
    path_bucket = clean_path(bucket_file.get())
    path_mofi =clean_path(model_confi.get())
    if desti_folder_build_lsh.get()=='':
        tmb.showwarning("Warning", "Destination folder entry is empty")
    elif bucket_file.get() == '':
        tmb.showwarning('Warning','Bucket file entry is empty')
    elif model_confi.get()=='':
        tmb.showwarning('Warning','Model configuration entry is empty')
    print ('Working ...')
    try:
        # Running the main function
        print ('Running',path_value_desti,path_bucket,path_mofi,uri_db_shrink_data.get())
    except Exception as ex:
        print (ex)

def shrink_data(tab2):
    # Execution button
    global desti_folder_shrink_data,bucket_file,model_confi,uri_db_shrink_data
    label_uri_db = Label(tab2,text='URI DB').pack()
    uri_db_shrink_data = StringVar()
    entry_box = Entry(tab2,textvariable=uri_db_shrink_data)
    entry_box.pack()

    theLabel = Label(tab2, text='Choose the bucket file').pack()
    bucket_file = StringVar()
    entry_box = Entry(tab2, textvariable=bucket_file).pack()
    button_browse_bucket = Button(tab2, text='Find bucket', command=partial(browse_button_shrink_data,'bucket'))
    button_browse_bucket.pack()

    theLabel = Label(tab2, text='Choose the model configuration file').pack()
    model_confi = StringVar()
    entry_box = Entry(tab2, textvariable=model_confi).pack()
    button_browse_confi = Button(tab2, text='Find model configuration', command=partial(browse_button_shrink_data,'configuration'))
    button_browse_confi.pack()

    theLabel = Label(tab2, text='Choose the destination folder').pack()
    desti_folder_shrink_data = StringVar()
    entry_box = Entry(tab2, textvariable=desti_folder_shrink_data).pack()
    button_browse_des = Button(tab2, text='Browse', command=partial(browse_button_shrink_data,'destination'))
    button_browse_des.pack()

    button_exe_shrink = Button(tab2, text='Execute', command=exeBut_ShrinkData)
    button_exe_shrink.pack()
    button_stop_exe_shrink = Button(tab2, text='Stop', command=stop_executing)
    button_stop_exe_shrink.pack()

# def loading():
#     try:
#         downloaded.set(next(files))  # update the progress bar
#         window.after(1, loading)  # call this function again in 1 millisecond
#     except StopIteration:
#         # the files iterator is exhausted
#         # window.destroy()
#         print ('Done')

if __name__=='__main__':
    multiprocessing.freeze_support() # To solve opening many windows caused by multiprocessing

    # Create a window
    window = Tk()
    window.title('BHXH')
    window.geometry("400x320")
    # Chia nhiem vu dong window (mac dinh la ko can nhung vi yeu cau bai toan nen phai tao bang tay)
    window.protocol('WM_DELETE_WINDOW', close_app)

    # Khoi tao bien chua tab
    tabControl = ttk.Notebook(window)

    # Khoi tao tab 1
    # Tao khung cho tab 1
    tab1 = ttk.Frame(tabControl)
    # Nhet  khung cua tab 1 vao trong tabControl
    tabControl.add(tab1, text='Build LSH')

    build_LSH(tab1)
    button_browse_build_lsh = Button(tab1, text='Browse', command=browse_button_build_LSH,state=NORMAL)
    button_browse_build_lsh.pack()

    button_execute = Button(tab1, text='Execute', command=exeBut_buildLSH,state=NORMAL)
    button_execute.pack()

    button_stop_build = Button(tab1, text='Stop', command=stop_executing,state=DISABLED)
    button_stop_build.pack()


    frame = Frame(tab1, relief=RAISED, borderwidth=1)
    frame.pack(fill=BOTH, expand=True)
    # Cai comment: la progress bar tang dan theo thoi gian
    # files = iter(np.arange(1, 10000))
    # downloaded = IntVar()
    # progress = ttk.Progressbar(frame, orient='horizontal', maximum=10000, variable=downloaded, mode='determinate')
    progress = ttk.Progressbar(frame, orient='horizontal', mode='indeterminate')
    progress.pack(fill=BOTH)

    # Khoi tao tab 2
    tab2 = ttk.Frame(tabControl)
    tabControl.add(tab2, text='Shrink Data')
    shrink_data(tab2)

    # Nhet cac tab vao trong window 1 cach dep de nhat
    tabControl.pack(expan=1, fill='both')

    window.quit()
    window.mainloop()
