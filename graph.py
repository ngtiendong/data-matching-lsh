import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate
import pickle

# fig = plt.figure()  # an empty figure with no axes

x = np.array([100, 200, 500, 800, 1000, 2000, 5000, 10000, 15000, 20000])

with open('time_origin.pkl', 'rb') as f:
    origin = np.array(pickle.load(f))

with open('time_lshcore.pkl', 'rb') as f:
    core = np.array(pickle.load(f))
    print(core)


f1 = interpolate.interp1d(x, origin[0], kind='cubic', fill_value="extrapolate")
f2 = interpolate.interp1d(x, origin[1], kind='cubic', fill_value="extrapolate")
f3 = interpolate.interp1d(x, origin[2], kind='cubic', fill_value="extrapolate")

x_new = np.arange(0, 1000000, 10000)
ynew_0 = f1(x_new)
ynew_1 = f2(x_new)
ynew_2 = f3(x_new)

f4 = interpolate.interp1d(x, core[0], kind='cubic', fill_value="extrapolate")
f5 = interpolate.interp1d(x, core[1], kind='cubic', fill_value="extrapolate")
f6 = interpolate.interp1d(x, core[2], kind='cubic', fill_value="extrapolate")

ynew_4 = f4(x_new)
ynew_5 = f5(x_new)
ynew_6 = f6(x_new)


fig, axs = plt.subplots(3, 1)
axs[0].plot(x_new, ynew_0, 'b', label='Ram')
axs[0].plot(x_new, ynew_4, 'r', label='File')
axs[0].set_title("Time consuming for hashing LSH")
axs[0].legend()

# plt.plot(x_new, ynew_2, 'y', label='Query')
axs[1].plot(x_new, ynew_1, 'b', label='Ram')
axs[1].plot(x_new, ynew_5, 'r', label='File')
axs[1].set_title("Time consuming for building LSH")
axs[1].legend()
# plt.plot(x_new, ynew_6, '--', 'y', label='Query')

axs[2].plot(x_new, ynew_2, 'b', label='Ram')
axs[2].plot(x_new, ynew_6, 'r', label='File')
axs[2].set_title("Time consuming for querying LSH")
axs[2].legend()

fig.tight_layout()

plt.show()



