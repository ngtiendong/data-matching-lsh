#!/usr/bin/env python
# coding: utf-8

import numpy as np
import pandas as pd
import random
import re
import time
import pickle
from datasketch import MinHash, MinHashLSH
from multiprocessing import Pool, cpu_count
from functools import partial
import mmh3
import _pickle as cPickle
import os

# INITIALIZE DATA
def prepare_to_extract_data(db):
    #Read CSV data
    db = db.iloc[2:]
    db.head()
    db['NGAY'] = db['NGAY'].astype('str').apply(lambda x: "0"+x if len(x) == 1 else x)
    db['THANG'] = db['THANG'].astype('str').apply(lambda x: "0"+x if len(x) == 1 else x)
    db['FULL_NAME'] = db['HO'] + ' ' + db['DEM']+ ' ' + db['TEN']
    db['BIRTHDAY'] = db['NGAY'] + db['THANG'] + db['NAM'].astype('str')
    db['KS'] = db['MA_TINH_KS'] + ' ' + db['MA_HUYEN_KS']+ ' ' + db['MA_XA_KS'].astype('str')
    db['IDENTIFY'] = db['SO_CMTND'].astype('str').apply(lambda x: "" if x.find("-") != -1 else x)
    data = db[['FULL_NAME', 'BIRTHDAY', 'KS', 'IDENTIFY']].copy()
    data.to_csv('./data/test_full_in_progress.csv', index=False)
    return './data/test_full_in_progress.csv'

def prepare_data(file,nrows):
    db = pd.read_csv(file,nrows=nrows)
    db['IDENTIFY'] = db['IDENTIFY'].astype('str')
    db['BIRTHDAY'] = db['BIRTHDAY'].astype('str')
    db['IDENTIFY'] = db['IDENTIFY'].astype('str').apply(lambda x: "" if x=="nan" or x=="SCMTND" else x)
    return db

# Phuc
# def ngrams(text, n):
#     word2  = np.array([[x[i:i+n] for i in range(0,len(x)-(n-1),1)] for x in text])
#     return word2
#
# def preprocess(datablock):
#     list_rows = []
#     for field in datablock.columns:
#         if str(datablock[field].dtype) == 'int64' or str(datablock[field].dtype)=='float64':
#             continue
#         text=datablock[field].replace(r'[^\w\s]','')
#         tokens = np.array([x.lower() if isinstance(x, str) else x for x in text])
#         split_token = ngrams(tokens,2)
#         if len(list_rows) == 0:
#             list_rows=split_token
#         else:
#             list_rows=np.add(list_rows,split_token)
#     return list_rows
#
# def preprocess_one_record(row):
#     list_tokens = []
#     #     print(row, type(row))
#     for field in row:
#         # print(field, type(field))
#         if type(field) is float:
#             continue
#         text = re.sub(r'[^\w\s]', '', field)
#         tokens = text.lower()
#         list_tokens += [tokens[i:i + 2] for i in range(0, len(tokens) - (2 - 1), 1)]
#     return list_tokens
#
# def _hash_func(d):
#     return mmh3.hash(d, signed=False)
#
# def hashing(row,perms):
#     m = MinHash(num_perm=perms, hashfunc=_hash_func)
#     for s in row:
#         m.update(s.encode('utf8'))
#     m.hashvalues= m.hashvalues.astype(np.int32)
#     # if np.array_equal(m.hashvalues.astype(np.int32),m.hashvalues)==False:
#     #     print (s)
#     #     print (m.hashvalues)
#     return m
#
# def hash_not_vectorize(rows,perms):
#     minhash =[]
#     for row in rows.tolist():
#         m = hashing(row,perms)
#         minhash.append(m)
#     return minhash
#
# def create_minhash(perms,data_block):
#     rows =preprocess(data_block)
#     minhash = hash_not_vectorize(rows,perms)
#     return minhash

def ngrams(text, n):
    words = [text[i:i + n] for i in range(0, len(text) - (n - 1), 1)]
    return words

def preprocess(row):
    list_tokens = []
    #     print(row, type(row))
    for field in row:
        # print(field, type(field))
        if type(field) is float:
            continue
        text = re.sub(r'[^\w\s]', '', field)
        tokens = text.lower()
        list_tokens += ngrams(tokens, 2)
    #     print(list(dict.fromkeys(list_tokens)))
    return list_tokens

def _hash_func(d):
    return mmh3.hash(d, signed=False)

def minhash_one_row(index, a, b, c, d, perms):
    global counter
    tokens = preprocess([a, b, c, d])
    m = MinHash(num_perm=perms, hashfunc=_hash_func, ID=index)
    for s in tokens:
        m.update(s.encode('utf8'))
    # m.hashvalues = m.hashvalues.astype(np.uint32)
    return m

def create_minhash(perms, data_block):
    return np.vectorize(minhash_one_row)(data_block.index, data_block['FULL_NAME'], data_block['BIRTHDAY'],
                                            data_block['KS'], data_block['IDENTIFY'], perms)

def multi_process_minhash(data, perms):
    global time_hash
    start = time.time()
    cores = cpu_count()
    df_split = np.array_split(data, cores, axis=0)
    pool = Pool(cores)
    func = partial(create_minhash, perms)
    all_minhash = np.concatenate(pool.map(func, df_split))
    pool.close()
    pool.join()
    duration = time.time() - start
    print('Done creating minhash:', duration)
    time_hash.append(duration)
    return all_minhash

def write_to_folder(lsh):
    global time_write
    start = time.time()
    with open("./lsh.pkl", "wb") as f:
        pickle.dump(lsh, f)
    duration = time.time()-start
    print ('Done writing to pickle',duration)
    time_write.append(duration)

def insert_minhash(session, minhash_sub_block):
    for idx, v in enumerate(minhash_sub_block):
        session.insert(v.ID, v)

def index_minhash_threshold(data, perms,flag,thresholds):
    global time_build_lsh
    if flag == '':
        lsh = MinHashLSH(threshold=thresholds, num_perm=perms)
    else:
        with open('./lsh.pkl', 'rb') as f:
            start = time.time()
            lsh = pickle.load(f)
            print ('Done loading from pkl:',time.time()-start)

    with lsh.insertion_session() as session:
        minhash = multi_process_minhash(data, perms)
        start = time.time()
        df_minhash_split = np.array_split(minhash, cpu_count(), axis=0)
        pool = Pool(cpu_count())
        func = partial(insert_minhash, session)
        pool.map(func, df_minhash_split)
        pool.close()
        pool.join()
        duration = time.time()-start
        print ('Done inserting to LSH:',duration)
        time_build_lsh.append(duration)
    write_to_folder(lsh)
    return lsh

def predict(record, database, perms, lsh, num_results=0):
    global time_query
    start_time = time.time()

    tokens = preprocess(record)
    m = MinHash(num_perm=perms, hashfunc=_hash_func)
    for s in tokens:
        m.update(s.encode('utf8'))

    if num_results == 0:
        idx_array = np.array(lsh.query(m))
    else:
        idx_array = np.array(lsh.query(m, num_results))
    if len(idx_array) == 0:
        return None  # if your query is empty, return none

    result = database.iloc[idx_array]

    print('It took %s seconds to query forest.' % (time.time() - start_time))
    time_query.append((time.time() - start_time))
    return result

# def predict(record, database, perms, lsh, num_results=0):
#     tokens = preprocess(record)
#     m = MinHash(num_perm=perms, hashfunc=_hash_func)
#     for s in tokens:
#         m.update(s.encode('utf8'))
#     if num_results == 0:
#         idx_array = np.array(lsh.query(m))
#         #print(idx_array)
#     else:
#         idx_array = np.array(lsh.query(m, num_results))
#         #print(idx_array)
#     if len(idx_array) == 0:
#         return None # if your query is empty, return none
#     result = database.iloc[idx_array]
#    # print('It took %s seconds to query forest.' %(time.time()-start_time))
#     return result
#
# def comparision_testing(permutations):
#     data_1 = {'FULL_NAME' : "Nguyen Van Dong",
#             'BIRTHDAY' : "30121993",
#             'KS' : "01TTL 231VH 8993",
#             'IDENTIFY' : "123456789"}
#     data_2 = {'FULL_NAME' : "Nguyen Van Dong",
#             'BIRTHDAY' : "30121992",
#             'KS' : "01TTL 231VH 8993",
#             'IDENTIFY' : "123456789"}
#     df_add = pd.DataFrame([data_1, data_2])
#
#     tokens_1 = preprocess(df_add.loc[0])
#     tokens_2 = preprocess(df_add.loc[1])
#
#     m1 = MinHash(num_perm=permutations)
#     print(tokens_1)
#     for s in tokens_1:
#         m1.update(s.encode('utf8'))
#     print(m1.digest().shape, len(tokens_1))
#     m2 = MinHash(num_perm=permutations)
#     print(tokens_2)
#     for s in tokens_2:
#         m2.update(s.encode('utf8'))
#
#     print(m2.digest().shape, len(tokens_2))
#
#     print("Estimated Jaccard for data1 and data2 is", m1.jaccard(m2))
#     s1 = set(data_1)
#     s2 = set(data_2)
#     actual_jaccard = float(len(s1.intersection(s2)))/float(len(s1.union(s2)))
#     print("Actual Jaccard for data1 and data2 is", actual_jaccard)

def query(db, permutations, lsh):
    # Query part
    data = {'FULL_NAME': "Trần Văn Minh",
            'BIRTHDAY': "10011916",
            'KS': "52TTT 053HH 18800",
            'IDENTIFY': "52216886179"}

    df = pd.DataFrame(data, index=[0])
    result = predict(df.loc[0],db, permutations, lsh=lsh)
    print('\n Top Recommendation(s) 2 is(are) \n', result)

def main():
    global time_hash, time_build_lsh, time_write, time_query
    arr = [10000]
    permutations = 256
    threshold = 0.6
    start_build= time.time()
    print (time.strftime('%X %x'))
    i = 0
    # for df in pd.read_csv('./data/new_record.csv',chunksize=200000,nrows=200000):
    for k in arr:
        time_chunk = time.time()
        #df = pd.read_csv('./data/new_record.csv',nrows=k)
        # file = prepare_to_extract_data(df)
        db = prepare_data('./data/test_full.csv',nrows=k)
        print ('Current chunksize:',i)
        if i == 0:
            lsh=index_minhash_threshold(db,permutations,'',threshold)
        else:
            lsh=index_minhash_threshold(db,permutations,'append',threshold)
        i+=1
        query(db, permutations, lsh)
        print ('Finish',i,'process in:',time.time()-time_chunk)

    # with open('time_lshcore_measure.pkl', 'wb') as f:
    #     pickle.dump(np.concatenate([time_hash, time_build_lsh, time_write, time_query]).reshape(4, len(arr)), f)

    print ('Finish building',time.time()-start_build)

if __name__=='__main__':
    time_hash, time_build_lsh, time_write, time_query = [], [], [], []
    main()

