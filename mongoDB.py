import pandas as pd
from pymongo import MongoClient

def connectDB():
    try:
        conn= MongoClient('localhost',27017)
        print ('Connect successfully')
    except:
        print ('Connection fails')
    # Database
    db = conn['LSH']
    # or
    db = conn.LSH
    # MongoDB stores data in the form of collection (not table) but we can understand those collections as tables
    collection2 = db['LSH']
    # Data in MongoDB is stored in dictionary
    emp_rec1 = {
        "name": "Mr.Geek",
        "eid": 24,
        "location": "wer"
    }
    emp_rec2 = {
        "name": "Mr.Shaurya",
        "eid": 14,
        "location": "dsf"
    }
    # Insert a single datum
    # datum = collection.insert_one(emp_rec1)
    # print (datum,datum.inserted_id)

    # Insert a list of data
    data = collection2.insert_many([emp_rec1,emp_rec2])


def main():
    connectDB()

if __name__=='__main__':
    main()