import numpy as np
import sys
from tqdm import tqdm
import pickle
from objsize import get_deep_size

def np_performance():
    arr = np.empty(50000,dtype=object)
    total= 0
    for i in tqdm(range(50000)):
        arr[i]=np.random.randint(1000000,5000000,size=4)
    print ('Size',arr.size*arr.itemsize/(1024**2),'MiB')
def dict_performance():
    dic = dict()
    size = sys.getsizeof(dic)
    for i in tqdm(range(50000)):
        dic[i]=np.random.randint(1000000,5000000,size=4)
        size += sum(map(sys.getsizeof, dic.values())) + sum(map(sys.getsizeof, dic.keys()))
    print ('Size',size/(1024**2),'MiB')

def check_pkl():
    with open('./data/iteration1.pkl','rb') as f:
        dic = pickle.load(f)
    size_dic = get_deep_size(dic)
    np_dic = np.array([list([k]+list(v)) for k,v in dic.loop().items()])
    dic = None
    del dic
    size_np_dic = np_dic.nbytes
    size_np_dic2 = get_deep_size(np_dic)
    print (size_dic,size_np_dic,size_np_dic2)
# check_pkl()
#
#
# import psutil
#
#
# def main():
#     '''Process kill function'''
#     for proc in psutil.process_iter():
#         # check whether the process name matches
#         print(proc.name())
#         if any(procstr in proc.name() for procstr in['Python']):
#             print(f'Killing {proc.name()}')
#             proc.kill()
#
#
# - 3 van de:
# + Create app

from tkinter import *

fenster = Tk()
fenster.title("Window")

def switch():
    print (b1)
    if b1["state"] == "normal":
        b1["state"] = "disabled"
        b2["text"] = "enable"
    else:
        b1["state"] = "normal"
        b2["text"] = "disable"

def testing():
    print ('Working')

#--Buttons
b1 = Button(fenster, text="Button", height=5, width=7,command=testing)
b1.grid(row=0, column=0)

b2 = Button(text="disable", command=switch)
b2.grid(row=0, column=1)

fenster.mainloop()