
import pandas as pd
from multiprocessing import Pool,cpu_count,Manager
import time
from functools import partial
import random
import numpy as np
import pandas as pd
from multiprocessing import *
import ctypes
from tqdm import tqdm
import os
from dask import dataframe as dd

def get_df():
    start =time.time()
    # df = pd.read_feather('../dataBHXH/60mil.feather')
    # ddf=dd.from_pandas(df,npartitions=1)
    ddf = dd.read_csv('../dataBHXH/new_record_fake_120.csv', blocksize=1000000)
    print ('Load into RAM',time.time()-start)
    return ddf

def get_random():
    np.random.seed(int.from_bytes(os.urandom(4), byteorder='little'))
    key = random.randint(0, 60000000)
    value = np.array(random.sample(range(60000000), 18000))
    return key,value

def compare(df,i):
    key,value = get_random()
    k = df.iloc[[key],'NGAY_SINH']
    v = df.iloc[value,'NGAY_SINH']
    return k,v

def compare_key_value(df):
    # Replace value with df[value]
    start = time.process_time()

    compare(df,1)


    time1 = time.process_time()-start
    print(time1)

def main():
    df = get_df()
    compare_key_value(df)


if __name__=='__main__':
    main()

