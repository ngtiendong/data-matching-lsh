import numpy as np
import pandas as pd
import feather
import re
import os
import time
import pickle
import mmh3
from multiprocessing import Pool, cpu_count
from functools import partial
from lshcore import MinHash, MinHashLSHForest, MinHashLSH, LeanMinHash
import itertools
from storage.storage import (ordered_storage, unordered_storage, _random_name)
from tqdm import tqdm
import sys
import gen_pairs
cpu_count = cpu_count()


def ngrams(text, n):
    words = [text[i:i + n] for i in range(0, len(text) - (n - 1), 1)]
    return words


def preprocess(row):
    list_tokens = []
    for field in row:
        if type(field) is float:
            continue
        text = re.sub(r'[^\w\s]', '', field)
        tokens = text.lower()
        list_tokens += ngrams(tokens, 2)
    return list_tokens


def _hash_func(d):
    return mmh3.hash(d, signed=False)


def sizeof_fmt(num, suffix='B'):
    ''' by Fred Cirera,  https://stackoverflow.com/a/1094933/1870254, modified'''
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(num) < 1024.0:
            return "%3.1f %s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f %s%s" % (num, 'Yi', suffix)


def minhash_one_row(index, a, b, c, perms):
    tokens = preprocess([a, b, c])
    m = MinHash(num_perm=perms, hashfunc=_hash_func, ID=index)
    for s in tokens:
        m.update(s.encode('utf8'))
    return LeanMinHash(m)


def create_minhash(perms, data_block):
    try:
        return np.vectorize(minhash_one_row)(data_block.index, data_block['FULL_NAME'], data_block['BIRTHDAY'],
                                             data_block['KS'],
                                             #  data_block['IDENTIFY'],
                                             perms)
    except Exception:
        return []


def multi_process_minhash(data, perms):
    print('Number of CPUs: ' + str(cpu_count))
    df_split = np.array_split(data, cpu_count, axis=0)
    pool = Pool(cpu_count)
    func = partial(create_minhash, perms)
    all_minhash = np.concatenate(pool.map(func, df_split))
    pool.close()
    pool.join()
    return all_minhash


def index_minhash_threshold(chunk, perms, last_index):
    global time_hash, time_build_lsh, lsh

    start_time = time.time()

    # List Lean min hash object
    minhash = multi_process_minhash(chunk, perms)

    print("Local variable at function index_minhash_threshold: ")
    for name, size in sorted(((name, sys.getsizeof(value)) for name, value in locals().items()), key=lambda x: -x[1])[:10]:
        print("{:>30}: {:>8}".format(name, sizeof_fmt(size)))

    timing = time.time() - start_time
    print('It took %s seconds to hash.' % timing)

    with lsh.insertion_session() as session:
        time_hash.append(timing)
        with tqdm(total=minhash.size) as pbar:
            for idx, m in enumerate(minhash):
                session.insert(idx + last_index, m)
                pbar.update(1)

    # with open("lsh_test.pkl", "wb") as f:
    #     pickle.dump(lsh, f)

    timing_lsh = time.time() - start_time
    print('It took %s seconds to build minhash lsh.' % timing_lsh)
    time_build_lsh.append(timing_lsh)


def predict(record, database, perms, lsh, num_results=0):
    '''
    num_results = 0: MinhashLSH
    num_results # 0: MinhashForest
    '''
    global time_query
    start_time = time.time()

    tokens = preprocess(record)
    m = MinHash(num_perm=perms, hashfunc=_hash_func)
    for s in tokens:
        m.update(s.encode('utf8'))

    if num_results == 0:
        idx_array = np.array(lsh.query(m))
    else:
        idx_array = np.array(lsh.query(m, num_results))
    if len(idx_array) == 0:
        return None  # if your query is empty, return none

    result = database.iloc[idx_array]

    print('It took %s seconds to query forest.' % (time.time() - start_time))
    time_query.append((time.time() - start_time))
    return result


def clean_field_data_csv(path):
    # Read CSV data
    db = pd.read_csv(path)
    del db['Unnamed: 0']
    db = db.iloc[2:]
    # del db['LABEL']
    db.head()
    db['NGAY'] = db['NGAY'].astype('str').apply(
        lambda x: "0" + x if len(x) == 1 else x)
    db['THANG'] = db['THANG'].astype('str').apply(
        lambda x: "0" + x if len(x) == 1 else x)
    db['FULL_NAME'] = db['HO'] + ' ' + db['DEM'] + ' ' + db['TEN']
    db['BIRTHDAY'] = db['NGAY'] + db['THANG'] + db['NAM'].astype('str')
    db['KS'] = db['MA_TINH_KS'] + ' ' + db['MA_HUYEN_KS'] + \
        ' ' + db['MA_XA_KS'].astype('str')
    db['IDENTIFY'] = db['SO_CMTND'].astype('str').apply(
        lambda x: "" if x.find("-") != -1 else x)
    data = db[['FULL_NAME', 'BIRTHDAY', 'KS', 'IDENTIFY']].copy()
    data.to_csv('test_full.csv', index=False)
    data.head()


def read_cleaned_dataframe(path, dataset):
    # clean_field_data_csv(path)
    # Read cleaned CSV
    db = pd.read_csv(path)
    db = db[:dataset]
    print(len(db))
    db['IDENTIFY'] = db['IDENTIFY'].astype('str')
    db['BIRTHDAY'] = db['BIRTHDAY'].astype('str')
    db['IDENTIFY'] = db['IDENTIFY'].astype('str')
    return db


def check_combination():
    with open('lsh.pkl', 'rb') as f:
        lsh_data = pickle.load(f)
        start = time.time()
        check_combinations = unordered_storage(
            {'type': 'dict'}, name='check_combinations')
        number_id = []
        j = 0
        for hashtable in lsh_data.hashtables:
            count = 0
            for key, value in hashtable.loop().items():
                length_value = len(value)
                if length_value > 1:
                    sorted_value = sorted(value)
                    for idx in range(length_value-1):
                        try:
                            temp_val = check_combinations.get(
                                sorted_value[idx], set())
                            temp_val.update(sorted_value[idx+1:])
                            check_combinations[sorted_value[idx]] = temp_val

                        except Exception as e:
                            print(e)

                        j += length_value-idx
        i = 0
        for k, v in check_combinations.items():
            i += len(v)
        print(i)
        print(j)

        print('It took %s seconds to build check combinations.' %
              (time.time() - start))


def building_lsh():
    global lsh
    global threshold
    global permutations

    # Data

    # i, j, start time
    i, j = 0, 0
    start_build = time.time()
    start_time = time.strftime('%X %x')

    data_path = input("Enter data path: ")
    data = pd.read_csv(data_path, dtype='str',
                       chunksize=1000000, low_memory=True)
    for db in data:
        for name, size in sorted(((name, sys.getsizeof(value)) for name, value in locals().items()),
                                 key=lambda x: -x[1])[:10]:
            print("{:>30}: {:>8}".format(name, sizeof_fmt(size)))

        print('First loop started at', start_time)
        start = time.time()
        print("i: {:d}".format(i))
        print("j: {:d}".format(j))
        # Build LSH data structure
        index_minhash_threshold(db, perms=permutations, last_index=j)
        write_detail_record(db, 1000, j)
        j += db.shape[0]

        print('LSH Size', sys.getsizeof(lsh))
        print('all Hashtable Size', sys.getsizeof(lsh.hashtables))
        print("hashtable size", sys.getsizeof(lsh.hashtables[0].loop()))

        print('End chunk', i, 'in', time.time() - start)
        i += 1
        if i % 5 == 0:
            for idx, hashtable in enumerate(lsh.hashtables):
                if not os.path.exists(f'./hashtables/hashtables{idx}'):
                    os.makedirs(f'./hashtables/hashtables{idx}')
                with open(f'./hashtables/hashtables{idx}/iteration{i//10}.pkl', 'wb') as f:
                    pickle.dump(hashtable, f)
            print('waiting for releasing memory')
            for hashtable in lsh.hashtables:
                del hashtable._dict
            lsh = MinHashLSH(threshold=threshold, num_perm=permutations)
            time.sleep(10)

    print("Final")
    for name, size in sorted(((name, sys.getsizeof(value)) for name, value in locals().items()),
                             key=lambda x: -x[1])[:10]:
        print("{:>30}: {:>8}".format(name, sizeof_fmt(size)))
    print(f'Finish building in', time.time() -
          start_build, 'at', time.strftime('%X %x'))


def write_detail_record(db, chunksize, last_index):
    if not os.path.exists('./data/detail_records'):
        os.makedirs('./data/detail_records')
    db.index = db.index
    for i in range(db.shape[0]//chunksize):
        fn = f'./data/detail_records/{last_index//chunksize + i}.feather'
        db.iloc[i * chunksize: (i+1) * chunksize].reset_index().to_feather(fn)


if __name__ == '__main__':
    time_hash, time_build_lsh, time_query = [], [], []

    # Number of Permutations
    permutations = 128

    # Number recommendations
    num_recommendations = 32

    # Threshold
    threshold = 0.5

    # LSH structure
    lsh = MinHashLSH(threshold=threshold, num_perm=permutations)

    building_lsh()
    gen_pairs.build_hashtable()
    # gen_pairs.gen_dict_pairs()
