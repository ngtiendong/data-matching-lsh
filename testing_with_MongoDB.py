#!/usr/bin/env python
# coding: utf-8

import numpy as np
import pandas as pd
import random
import re
import time
import pickle
from lshcore.minhashLSHMongoDB import MinHashLSH
from datasketch import MinHash
from multiprocessing import Pool, cpu_count
from functools import partial
import mmh3
# import pathos.multiprocessing as mp
from pymongo import MongoClient
from datasketch.experimental.aio.lsh import AsyncMinHashLSH

# INITIALIZE DATA
def prepare_to_extract_data(db):
    #Read CSV data
    db = db.iloc[2:]
    db.head()
    db['NGAY'] = db['NGAY'].astype('str').apply(lambda x: "0"+x if len(x) == 1 else x)
    db['THANG'] = db['THANG'].astype('str').apply(lambda x: "0"+x if len(x) == 1 else x)
    db['FULL_NAME'] = db['HO'] + ' ' + db['DEM']+ ' ' + db['TEN']
    db['BIRTHDAY'] = db['NGAY'] + db['THANG'] + db['NAM'].astype('str')
    db['KS'] = db['MA_TINH_KS'] + ' ' + db['MA_HUYEN_KS']+ ' ' + db['MA_XA_KS'].astype('str')
    db['IDENTIFY'] = db['SO_CMTND'].astype('str').apply(lambda x: "" if x.find("-") != -1 else x)
    data = db[['FULL_NAME', 'BIRTHDAY', 'KS', 'IDENTIFY']].copy()
    data.to_csv('test_full.csv', index=False)
    return './test_full.csv'

def prepare_data(file,nrows):
    db = pd.read_csv(file,nrows=nrows)
    db['IDENTIFY'] = db['IDENTIFY'].astype('str')
    db['BIRTHDAY'] = db['BIRTHDAY'].astype('str')
    db['IDENTIFY'] = db['IDENTIFY'].astype('str').apply(lambda x: "" if x=="nan" or x=="SCMTND" else x)
    return db

# COMMON FUNCTIONS
def ngrams(text, n):
    word2  = np.array([[x[i:i+n] for i in range(len(x)-1)] for x in text])
    return word2

def preprocess(datablock):
    list_rows = []
    for field in datablock.columns:
        if str(datablock[field].dtype) == 'int64' or str(datablock[field].dtype)=='float64':
            continue
        text=datablock[field].replace(r'[^\w\s]','')
        tokens = np.array([x.lower() if isinstance(x, str) else x for x in text])
        split_token = ngrams(tokens,2)
        if len(list_rows) == 0:
            list_rows=split_token
        list_rows=np.add(list_rows,split_token)
    return list_rows

# Let's use MurmurHash3.
def _hash_func(d):
    return mmh3.hash(d, signed=False)

def hashing(row,perms):
    m = MinHash(num_perm=perms, hashfunc=_hash_func)
    for s in row:
        m.update(s.encode('utf8'))
    return m

def hash_not_vectorize(rows,perms):
    minhash=[]
    for row in rows.tolist():
        m = hashing(row,perms)
        minhash.append(m)
    return minhash

def create_minhash(perms,data_block):
    rows =preprocess(data_block)
    minhash = hash_not_vectorize(rows,perms)
    return minhash

def multi_process_minhash(data, perms):
    start = time.time()
    cores = cpu_count()
    df_split = np.array_split(data, cores, axis=0)
    pool = Pool(cores)
    func = partial(create_minhash, perms)
    all_minhash = np.concatenate(pool.map(func, df_split))
    pool.close()
    pool.join()
    print('Done creating minhash:', time.time() - start)
    return all_minhash


def connectDB():
    try:
        conn= MongoClient('localhost',27017)
        print ('Connect successfully')
    except:
        print ('Connection fails')
    # Database
    db = conn['LSH']
    # or
    #db = conn.LSH

def index_minhash_threshold(data, perms,flag,threshold,last_df_index):
    if flag == '':
        lsh = MinHashLSH(
           threshold=threshold, num_perm=perms)
    else:
        with open('lsh.pkl', 'rb') as f:
            start = time.time()
            lsh = pickle.load(f)
            print ('Done loading from pkl:',time.time()-start)

    with lsh.insertion_session() as session:
        minhash = multi_process_minhash(data, perms)
        start = time.time()
        if flag =='':
            for i,m in enumerate(minhash):
                session.insert(i, m)
        else:
            for i, m in enumerate(minhash):
                lsh.insert(last_df_index + i, m)
        print ('Done inserting to LSH:',time.time()-start)
    # start = time.time()
    # with open("lsh.pkl", "wb") as f:
    #     pickle.dump(lsh, f)
    # print ('Done writing to pkl:',time.time()-start)
    return lsh

# def predict(record, database, perms, lsh, num_results=0):
#     tokens = preprocess(record)
#     m = MinHash(num_perm=perms, hashfunc=_hash_func)
#     for s in tokens:
#         m.update(s.encode('utf8'))
#     if num_results == 0:
#         idx_array = np.array(lsh.query(m))
#         #print(idx_array)
#     else:
#         idx_array = np.array(lsh.query(m, num_results))
#         #print(idx_array)
#     if len(idx_array) == 0:
#         return None # if your query is empty, return none
#     result = database.iloc[idx_array]
#    # print('It took %s seconds to query forest.' %(time.time()-start_time))
#     return result
#
# def comparision_testing(permutations):
#     data_1 = {'FULL_NAME' : "Nguyen Van Dong",
#             'BIRTHDAY' : "30121993",
#             'KS' : "01TTL 231VH 8993",
#             'IDENTIFY' : "123456789"}
#     data_2 = {'FULL_NAME' : "Nguyen Van Dong",
#             'BIRTHDAY' : "30121992",
#             'KS' : "01TTL 231VH 8993",
#             'IDENTIFY' : "123456789"}
#     df_add = pd.DataFrame([data_1, data_2])
#
#     tokens_1 = preprocess(df_add.loc[0])
#     tokens_2 = preprocess(df_add.loc[1])
#
#     m1 = MinHash(num_perm=permutations)
#     print(tokens_1)
#     for s in tokens_1:
#         m1.update(s.encode('utf8'))
#     print(m1.digest().shape, len(tokens_1))
#     m2 = MinHash(num_perm=permutations)
#     print(tokens_2)
#     for s in tokens_2:
#         m2.update(s.encode('utf8'))
#
#     print(m2.digest().shape, len(tokens_2))
#
#     print("Estimated Jaccard for data1 and data2 is", m1.jaccard(m2))
#     s1 = set(data_1)
#     s2 = set(data_2)
#     actual_jaccard = float(len(s1.intersection(s2)))/float(len(s1.union(s2)))
#     print("Actual Jaccard for data1 and data2 is", actual_jaccard)

# This cell to append new record to existed lsh
# def append_data_LSH(df_add,last_df_index,permutations):
#     with open('lsh.pkl', 'rb') as f:
#         lsh_threshold = pickle.load(f)
#         numpy_data = preprocess(df_add)
#         for tokens in numpy_data.tolist():
#             m = MinHash(num_perm=permutations)
#             for s in tokens:
#                 m.update(s.encode('utf8'))
#             lsh_threshold.insert(last_df_index, m)
#             last_df_index+=1
#     return lsh_threshold

# def query(df,db, permutations, lsh_threshold):
#     # Query part
#     # data = {'FULL_NAME': "BÃ¹i KhÃ¡nh Trang",
#     #         'BIRTHDAY': "15121981",
#     #         'KS': "26TTT 101HH 712",
#     #         'IDENTIFY': ""}
#     # df = pd.DataFrame(data, index=[0])
#     result_2 = predict(df, db, permutations, lsh=lsh_threshold)
#     print('\n Top Recommendation(s) 2 is(are) \n', result_2)

def main():
    permutations = 256
    threshold = 0.55
    start_build = time.time()
    print (start_build)
    i = 0
    j = 0
    arr = [1000]
    for k in arr:
    #for df in pd.read_csv('./data/test_full.csv',chunksize=1000, nrows=1000):
        time_chunk = time.time()
        # file = prepare_to_extract_data(df)
        # db = prepare_data(file)
        db = prepare_data('./data/test_full.csv', nrows=k)
        print ('Current chunksize:',i)
        if i == 0:
            lsh_threshold = index_minhash_threshold(db,permutations,'',threshold,j)
        else:
            lsh_threshold = index_minhash_threshold(db,permutations,'append',threshold,j)
        i+=1
        j+=db.shape[0]
        print ('Finish',i,'process in:',time.time()-time_chunk)
    print ('Finish building',time.time()-start_build)

if __name__=='__main__':
    main()


