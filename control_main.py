import optimize_lsh7_virtual_RAM as running
import time
import pandas as pd
import objgraph

def main():
    i = 0
    j = 0
    start_build = time.time()
    start_time = time.strftime('%X %x')
    print(start_time)
    data = pd.read_csv('../dataBHXH/test_full_120.csv', dtype='str', chunksize=1500000,low_memory=True)
    for db in data:
        print('First loop started at', start_time)
        print("START")
        start = time.time()
        print(i)
        time.sleep(10)
        objgraph.show_growth()
        if i == 0:
            running.main(db,j,False)
        else:
            running.main(db,j,True)
        objgraph.show_growth()
        # obb_lean_minhash = objgraph.by_type('LeanMinhash')
        # objgraph.show_backrefs([obb_lean_minhash], filename='sample-backref-graph.png')
        j += db.shape[0]
        print('End chunk', i, 'in', time.time() - start)
        i += 1

    print('Finish building in', time.time() - start_build, 'at', time.strftime('%X %x'))

if __name__=='__main__':
    main()