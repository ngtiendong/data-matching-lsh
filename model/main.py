# Standard Record
# {'HO': 'Lưu', 'DEM': 'Văn', 'TEN': 'Thanh',
#  'SO_CMTND': '3249384318', 'NGAYSINH': '19491001'}

# TEST SHRINK DATA
import pickle
from multiprocessing import Pool, cpu_count
import numpy as np
from textdistance import jaro, jaccard
import json
from shrink import shrink
from utils import load_data, get_bucket_record
from tqdm import tqdm
import time
from datetime import datetime
from functools import partial
import glob
import os


def load_base_hashtable():
    hashtable_fns = glob.glob("../hashtables/*.pkl")
    hashtable_fns = sorted(hashtable_fns, key=lambda x: os.path.getsize)
    base_hashtable = hashtable_fns[0]
    with open(base_hashtable, 'rb') as f:
        hashtable = pickle.load(f)
        bucket_ids = hashtable.values()
        del hashtable
    return bucket_ids


def main():
    with open("config.json") as f:
        weights = json.load(f)
    start = time.time()
    bucket_ids = load_base_hashtable()

    func = partial(shrink_bucket_ids, weights=weights, threshold=0.9)
    num_processes = cpu_count()
    with Pool(num_processes) as pool:
        res = np.concatenate(pool.map(func, bucket_ids))
    end = time.time()
    print("running time", end-start)
    with open("res.pkl", 'wb') as f:
        pickle.dump(res, f)
# Jaro similarity (use for TEN)


def shrink_bucket_ids(bucket_ids, weights, threshold):
    records = get_bucket_record(bucket_ids, '../data/detail_records')
    print(bucket_ids[:10], datetime.now())
    res = shrink(records, weights, threshold)
    return res


if __name__ == '__main__':
    main()
