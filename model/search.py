import pandas as pd
import json
from utils import load_data
from model import sim_record


def search(df, sample, weights, threshold=0.7):
    result = []
    for _, row in df.iterrows():
        try:
            score = sim_record(row, sample, weights)
            if score > threshold:
                result.append(row)
        except ValueError:
            pass
    return result


# main
if __name__ == '__main__':
    # Read weight from json file
    with open('./data/config.json') as f:
        config = f.read()
    config = json.loads(config)

    # Load data
    path = './data/test_shrink_data.csv'
    df = load_data(path)
    sample = {'HO': 'Lưu', 'DEM': 'Văn', 'TEN': 'Thanh',
              'SO_CMTND': '3249384318', 'NGAYSINH': '19491001'}

    # Search
    res = search(df, sample, config)
    res = pd.DataFrame(res)
    res.to_csv('output/output.csv')
