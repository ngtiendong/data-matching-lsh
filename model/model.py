from cores import jaro_sim, jaccard_sim, distance_sim
from math import sin, cos, sqrt, atan2, exp
from datetime import date
import math


def sim_HO(s1, s2): return jaccard_sim(s1, s2)


def sim_DEM(s1, s2): return jaccard_sim(s1, s2)


def sim_TEN(s1, s2): return jaro_sim(s1, s2)


def sim_NOI_SINH(geo1, geo2, beta=0.0001):
    (lat1, long1) = geo1
    (lat2, long2) = geo2
    R = 6373.0
    dlong = long2 - long1
    dlat = lat2 - lat1
    a = (sin(dlat/2))**2 + cos(lat1) * cos(lat2) * (sin(dlong/2))**2
    c = 2 * atan2(sqrt(a), sqrt(1-a))
    distance = R * c
    return distance_sim(distance, beta)


def sim_NGAY_SINH(dob1, dob2, beta=0.0015):
    year1 = int(dob1[0:4])
    month1 = int(dob1[4:6])
    day1 = int(dob1[6:])
    year2 = int(dob2[0:4])
    month2 = int(dob2[4:6])
    day2 = int(dob2[6:])
    time1 = date(year1, month1, day1)
    time2 = date(year2, month2, day2)
    fdays = date.today() - time1
    ldays = date.today() - time2
    delta = abs(fdays - ldays).days

    score1 = distance_sim(delta, beta)
    score2 = jaccard_sim(dob1, dob2)
    return (score1 + score2)/2


def sim_SO_CMTND(s1, s2): return jaccard_sim(s1, s2)


def sim_record(record1, record2, weights):
    res = 0
    try:
        temp = 0
        for key in weights:
            score = globals()[f'sim_{key}'](record1[key], record2[key])
            temp += score * weights[key]
        res = temp
        res /= sum(weights.values())
    except Exception as e:
        pass
    return res
