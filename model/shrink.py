
from itertools import combinations
from multiprocessing import Pool, cpu_count
from model import sim_record
from functools import partial
import numpy as np


def custom_sim_record(records, weights):
    return sim_record(*records, weights)


def shrink(records, weights, threshold):
    if len(records) < 2:
        return []
    func = partial(custom_sim_record, weights=weights)
    pairs = combinations(records, 2)
    res = []
    counter = 0
    for pair in pairs:
        counter += 1
        sim = func(pair)
        if sim > threshold:
            res.append(pair)
    return res
