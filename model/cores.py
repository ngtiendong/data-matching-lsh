import numpy as np
import math
# Jaro similarity (use for TEN)


def jaro_sim(s1, s2):
    # return jaro.similarity(s1, s2)
    len_s1, len_s2 = len(s1), len(s2)
    match_bound = math.floor(max(len(s1), len(s2)) / 2) - 1
    matches = 0
    transpositions = 0
    for ch1 in s1:
        if ch1 in s2:
            pos1 = s1.index(ch1)
            pos2 = s2.index(ch1)
            if(abs(pos1-pos2) <= match_bound):
                matches += 1
                if(pos1 != pos2):
                    transpositions += 1

    if matches == 0:
        return 0
    else:
        return 1/3 * (matches/len_s1 + matches/len_s2 + (matches-transpositions//2) / matches)


# Distance Similarity (use for NOI_SINH, NGAY_SINH)
def distance_sim(delta, beta):
    ## Similarity = e^{-beta * delta}
    similarity = np.exp(-beta * delta)
    return similarity

# Jaccard Similarity (use for HO, DEM, CMTND)


def jaccard_sim(s1, s2):
    intersec_len = len(set(s1).intersection(set(s2)))
    uni_len = len(set(s1).union(set(s2)))
    return intersec_len/uni_len
