import googlemaps
import pandas as pd


def load_data(path):
    df = pd.read_csv(path)
    df = df[['HO', 'DEM', 'TEN', 'NGAY', 'THANG', 'NAM', 'SO_CMTND']]
    df['HO'] = df['HO'].astype(str)
    df['DEM'] = df['DEM'].astype(str)
    df['TEN'] = df['TEN'].astype(str)
    df.THANG = df.THANG.apply(lambda x: "{:02}".format(x))
    df.NGAY = df.NGAY.apply(lambda x: "{:02}".format(x))
    df['NGAYSINH'] = df['NAM'].map(
        str) + df['THANG'].map(str) + df['NGAY'].map(str)
    df['SO_CMTND'] = df['SO_CMTND'].astype(str)
    df = df[['HO', 'DEM', 'TEN', 'SO_CMTND', 'NGAYSINH']]
    return df


def get_bucket_record(bucket_ids, data_folder):
    bucket_ids = bucket_ids.astype(int)
    data_files = set(bucket_ids//1000)
    res = pd.DataFrame()
    for fn in data_files:
        try:
            df = pd.read_feather(f"{data_folder}/{fn}.feather")
            df.set_index('index',  inplace=True)
            ids = bucket_ids[bucket_ids//1000 == fn]
            res = pd.concat([res, df.loc[ids]])
        except:
            print(ids)
    return res.reset_index().to_dict('records')


def get_geocode(address):
    ggmaps_api_keys = 'AIzaSyCzaBNQ4yqs10AVQ8G3oUzLp9qgt9QzUr4'
    gmaps = googlemaps.Client(key=ggmaps_api_keys)
    geocode_result = gmaps.geocode(address)
    res = geocode_result[0]['geometry']['location']
    return res
