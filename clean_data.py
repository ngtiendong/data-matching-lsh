import pandas as pd
import os
#
def clean_field_data_csv(db):
    # Read CSV data
    # del db['LABEL']
    db['NGAY'] = db['NGAY'].apply(lambda x: "0" + x if len(x) == 1 else x)
    db['THANG'] = db['THANG'].apply(lambda x: "0" + x if len(x) == 1 else x)
    db['FULL_NAME'] = db['HO'] + ' ' + db['DEM'] + ' ' + db['TEN']
    db['BIRTHDAY'] = db['NGAY'] + db['THANG'] + db['NAM']
    db['KS'] = db['MA_TINH_KS'] + ' ' + db['MA_HUYEN_KS'] + ' ' + db['MA_XA_KS']
    db['IDENTIFY'] = db['SO_CMTND'].astype('str').apply(lambda x: "" if x.find("-") != -1 else x)
    data = db[['FULL_NAME', 'BIRTHDAY', 'KS', 'IDENTIFY']].copy()
    if not os.path.isfile('./data/clean_120.csv'):
        data.to_csv('./data/clean_120.csv', index=False)
    else:
        data.to_csv('./data/clean_120.csv', index=False, mode='a')

i=0
for df_fg in pd.read_csv('../dataBHXH/new_record_fake_120.csv',chunksize=5000000,dtype='str'):
    print(i)
    clean_field_data_csv(df_fg)
    i+=1
# df = pd.read_csv('./data/new_record_fake_120.csv',dtype='str',nrows=5000000)
# clean_field_data_csv(df)

# i = 0
# df2 = pd.read_csv('./data/clean_fake_120.csv',nrows=2000000,dtype='str')
# # for df2 in pd.read_csv('./data/new_record_fake_120.csv',chunksize=2000000):
# #     print (i)
# #
# #     i+=1
#
# df2.to_csv('./data/clean_2_mil_with_CMTND.csv',index=False)