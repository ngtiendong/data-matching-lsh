import random
from tqdm import tqdm
import time
import numpy as np
from multiprocessing import Pool,cpu_count
import pickle
import os
import itertools

# def gen_data(i):
#     np.random.seed(int.from_bytes(os.urandom(4), byteorder='little'))
#     key= random.randint(0,60000000)
#     value = np.array(random.sample(range(60000000),18000))
#     return [key,value]
#
# def multiproc():
#     s = range(58594)
#     pool = Pool(cpu_count())
#     result_np = pool.map(gen_data,s)
#     pool.close()
#     pool.join()
#     return result_np
#
# def pick_random():
#     start =time.time()
#     dic ={}
#     result=[]
#     for i in tqdm(itertools.repeat("spam", 32)):
#         r = multiproc()
#         result+=r
#         del r
#         time.sleep(5)
#     print ('Gen',time.time()-start)
#     start = time.time()
#     for token in result:
#         dic[list(token[0])]=token[1]
#     print ('Convert',time.time()-start)
#     return dic

def main():
    mylist=[1,2,3,4,5]
    for a, b in itertools.combinations(mylist, 2):
        print (a,b)

if __name__=='__main__':
    main()