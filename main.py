import numpy as np
import pandas as pd
import re
import time
import pickle
import mmh3
from multiprocessing import Pool, cpu_count
from functools import partial
from lshcore import MinHash, MinHashLSHForest, MinHashLSH, LeanMinHash
from sklearn.cluster import KMeans
import progressbar
import itertools
from storage.storage import (ordered_storage, unordered_storage, _random_name)

cpu_count = cpu_count()

def ngrams(text, n):
    words = [text[i:i + n] for i in range(0, len(text) - (n - 1), 1)]
    return words


def preprocess(row):
    list_tokens = []
    for field in row:
        if type(field) is float:
            continue
        text = re.sub(r'[^\w\s]', '', field)
        tokens = text.lower()
        list_tokens += ngrams(tokens, 2)
    return list_tokens


def _hash_func(d):
    return mmh3.hash(d, signed=False)


def minhash_one_row(index, a, b, c, d, perms):
    tokens = preprocess([a, b, c, d])
    m = MinHash(num_perm=perms, hashfunc=_hash_func, ID=index)
    for s in tokens:
        m.update(s.encode('utf8'))
    return LeanMinHash(m)


def create_minhash(perms, data_block):
    try:
        return np.vectorize(minhash_one_row)(data_block.index, data_block['FULL_NAME'], data_block['BIRTHDAY'],
                                                data_block['KS'], data_block['IDENTIFY'], perms)
    except Exception:
        return []


def multi_process_minhash(data, perms):
    print('Number of CPUs: ' + str(cpu_count))
    df_split = np.array_split(data, cpu_count, axis=0)
    pool = Pool(cpu_count)
    func = partial(create_minhash, perms)
    all_minhash = np.concatenate(pool.map(func, df_split))
    pool.close()
    pool.join()
    return all_minhash


def get_forest(data, perms):
    start_time = time.time()
    minhash = multi_process_minhash(data, perms)
    #     minhash = create_minhash(perms, data)

    print('It took %s seconds to hashing.' % (time.time() - start_time))

    forest = MinHashLSHForest(num_perm=perms)

    for i, m in enumerate(minhash):
        forest.add(i, m)

    forest.index()

    with open("forest.pkl", "wb") as f:
        pickle.dump(forest, f)

    print('It took %s seconds to build forest.' % (time.time() - start_time))

    return forest


def index_minhash_threshold(data, perms, threshold):
    global time_hash, time_build_lsh
    start_time = time.time()

    lsh = MinHashLSH(
        threshold=threshold, num_perm=perms)
    minhash = multi_process_minhash(data, perms)
    query = ""
    with progressbar.ProgressBar(max_value=len(minhash)) as bar:
        i, j = 0, 0
        with lsh.insertion_session() as session:
            # minhash = create_minhash(perms, data)
            timing = time.time() - start_time
            print('It took %s seconds to hashing.' % timing)
            time_hash.append(timing)
            # Old version 1 thread build lsh
            for idx, m in enumerate(minhash):
                session.insert(idx, m)
                i += 1
                # j += 1
                # if j == 10:
                #     neo4j.create_node_query(query)
                #     query = ""
                #     j = 0
                bar.update(i)
    # with open('test.txt', 'w') as f:
    #     f.write(query)


    with open("lsh_test.pkl", "wb") as f:
        pickle.dump(lsh, f)

    timing_lsh = time.time() - start_time
    print('It took %s seconds to build minhash lsh.' % timing_lsh)
    time_build_lsh.append(timing_lsh)
    return lsh


def insert_minhash(session, minhash_sub_block):
    for idx, v in enumerate(minhash_sub_block):
        session.insert(v.ID, v)


def predict(record, database, perms, lsh, num_results=0):
    '''
    num_results = 0: MinhashLSH
    num_results # 0: MinhashForest
    '''
    global time_query
    start_time = time.time()

    tokens = preprocess(record)
    m = MinHash(num_perm=perms, hashfunc=_hash_func)
    for s in tokens:
        m.update(s.encode('utf8'))

    if num_results == 0:
        idx_array = np.array(lsh.query(m))
    else:
        idx_array = np.array(lsh.query(m, num_results))
    if len(idx_array) == 0:
        return None  # if your query is empty, return none

    result = database.iloc[idx_array]

    print('It took %s seconds to query forest.' % (time.time() - start_time))
    time_query.append((time.time() - start_time))
    return result


def clean_field_data_csv(path):
    # Read CSV data
    db = pd.read_csv(path)
    del db['Unnamed: 0']
    db = db.iloc[2:]
    # del db['LABEL']
    db.head()
    db['NGAY'] = db['NGAY'].astype('str').apply(lambda x: "0" + x if len(x) == 1 else x)
    db['THANG'] = db['THANG'].astype('str').apply(lambda x: "0" + x if len(x) == 1 else x)
    db['FULL_NAME'] = db['HO'] + ' ' + db['DEM'] + ' ' + db['TEN']
    db['BIRTHDAY'] = db['NGAY'] + db['THANG'] + db['NAM'].astype('str')
    db['KS'] = db['MA_TINH_KS'] + ' ' + db['MA_HUYEN_KS'] + ' ' + db['MA_XA_KS'].astype('str')
    db['IDENTIFY'] = db['SO_CMTND'].astype('str').apply(lambda x: "" if x.find("-") != -1 else x)
    data = db[['FULL_NAME', 'BIRTHDAY', 'KS', 'IDENTIFY']].copy()
    data.to_csv('test_full.csv', index=False)
    data.head()


def read_cleaned_dataframe(path, dataset):
    # clean_field_data_csv(path)
    # Read cleaned CSV
    db = pd.read_csv(path)
    db = db[:dataset]
    print(len(db))
    db['IDENTIFY'] = db['IDENTIFY'].astype('str')
    db['BIRTHDAY'] = db['BIRTHDAY'].astype('str')
    db['IDENTIFY'] = db['IDENTIFY'].astype('str')
    return db


def build_lsh_structure(db, threshold, permutations, type="normal"):
    # Boost performance
    # a = np.vectorize(preprocess_2)(db['FULL_NAME'], db['BIRTHDAY'], db['KS'], db['IDENTIFY'])

    # Build LSH data structure
    if type == 'normal':
        lsh = index_minhash_threshold(data=db, perms=permutations, threshold=threshold)
    else:
        # Forest
        lsh = get_forest(db, permutations)

    # return lsh


def append_new_record_to_lsh(db, permutations):
    # This cell to append new record to existed lsh
    data_add = {'FULL_NAME': "Nguyen Tien Dong",
                'BIRTHDAY': "30121992",
                'KS': "01TTL 231VH 8993",
                'IDENTIFY': "123456789"}
    df_add = pd.DataFrame(data_add, index=[0])
    db.loc[len(db)] = df_add.iloc[0]
    # with open('forest.pkl', 'rb') as f:
    #     forest = pickle.load(f)
    #     tokens = preprocess(df_add.loc[0])
    #     m = MinHash(num_perm=permutations)
    #     for s in tokens:
    #         m.update(s.encode('utf8'))

    #     forest.add(db.shape[0]-1, m)
    #     forest.index()

    with open('lsh.pkl', 'rb') as f:
        lsh_threshold = pickle.load(f)
        tokens = preprocess(df_add.loc[0])
        m = MinHash(num_perm=permutations)
        for s in tokens:
            m.update(s.encode('utf8'))

        lsh_threshold.insert(db.shape[0] - 1, m)


def check_combination():
    with open('lsh.pkl', 'rb') as f:
        lsh_data = pickle.load(f)
        start = time.time()
        check_combinations = unordered_storage({'type': 'dict'}, name='check_combinations')
        j = 0
        for hashtable in lsh_data.hashtables:
            for key, value in hashtable.loop().items():
                if len(value) > 1:
                    for x in itertools.combinations(value, 2):
                        j += 1
                        t = sorted(list(x))
                        check_combinations.insert(t[0], t[1])

        i = 0
        for k, v in check_combinations.loop().items():
            i += len(v)
        print(i)
        print(j)

        print('It took %s seconds to build check combinations.' % (time.time() - start))


if __name__ == '__main__':
    # Read dataframe
    # clean_field_data_csv('data/new_record.csv')
    time_hash, time_build_lsh, time_query = [], [], []
    # arr = [100, 200, 500, 800, 1000, 2000, 5000, 10000, 15000, 20000]
    arr = [7]

    # Number of Permutations
    permutations = 128

    # Number recommendations
    num_recommendations = 32

    # Threshold
    threshold = 0.7

    for i in arr:
        db = read_cleaned_dataframe(path="data/test_full.csv", dataset=i)

        # Build LSH data structure
        lsh = index_minhash_threshold(db, threshold=threshold, perms=permutations)
        print("Num hashtable: {:d}".format(len(lsh.hashtables)))

        # Query
        data = {'FULL_NAME': "Trần Văn Minh",
                'BIRTHDAY': "10011916",
                'KS': "52TTT 053HH 18800",
                'IDENTIFY': "52216886179"}

        df = pd.DataFrame(data, index=[0])
        result = predict(df.loc[0], db, permutations, lsh=lsh)

        print('\n Top Recommendation(s) is(are) \n', result)

    # with open('time_test.pkl', 'wb') as f:
    #     pickle.dump(np.concatenate([time_hash, time_build_lsh, time_query]).reshape(3, len(arr)), f)

