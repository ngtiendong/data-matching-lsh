from datasketch.hyperloglog import HyperLogLog, HyperLogLogPlusPlus
from lshcore.minhash import MinHash
from datasketch.b_bit_minhash import bBitMinHash
from lshcore.minhashLshRam import MinHashLSH
from datasketch.weighted_minhash import WeightedMinHash, WeightedMinHashGenerator
from lshcore.minhashForest import MinHashLSHForest
from datasketch.lshensemble import MinHashLSHEnsemble
from datasketch.lean_minhash import LeanMinHash
from datasketch.hashfunc import sha1_hash32

# Alias
WeightedMinHashLSH = MinHashLSH
WeightedMinHashLSHForest = MinHashLSHForest

# Version
from datasketch.version import __version__