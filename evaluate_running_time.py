import glob
import os
import pickle


def compute_num_comparision(bucket_size):
    return bucket_size * (bucket_size-1)/2


def main():

    hashtables = sorted(glob.glob('./hashtables/*.pkl'), key=os.path.getsize)

    base_hashtable = hashtables[0]

    with open(base_hashtable, 'rb') as f:
        hashtable = pickle.load(f)

    num_comparisions = [compute_num_comparision(
        len(i)) for i in hashtable.values()]

    print("Expected Number of Comparisions in base hashtable:", num_comparisions)
    print("Expected Running time (h) for shrinking base hashtable (one core):",
          num_comparisions*2e-5/3600)
    print("Expected Running time (days) for shrinking base hashtable (160 core):",
          num_comparisions*2e-5/3600/24)


if __name__ == "__main__":
    main()
