import time
import os
import pandas as pd


def write_detail_record(db, chunksize, last_index):
    if not os.path.exists('./data/detail_records'):
        os.makedirs('./data/detail_records')
    # db.index = db.index + last_index
    for i in range(db.shape[0]//chunksize):
        fn = f'./data/detail_records/{last_index//chunksize + i}.feather'
        db.iloc[i * chunksize: (i+1) * chunksize].reset_index().to_feather(fn)


def main():
    i, j = 0, 0
    start_build = time.time()
    start_time = time.strftime('%X %x')

    data_path = input("Enter data path: ")
    data = pd.read_csv(data_path, dtype='str',
                       chunksize=1000000, low_memory=True)
    for db in data:

        print('First loop started at', start_time)
        print("i: {:d}".format(i))
        print("j: {:d}".format(j))
        # Build LSH data structure
        write_detail_record(db, 1000, j)
        j += db.shape[0]
    print(f'Finish building in', time.time() -
          start_build, 'at', time.strftime('%X %x'))


if __name__ == '__main__':
    main()
