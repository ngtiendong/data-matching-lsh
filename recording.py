import psutil
import time
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('darkgrid')
def main():
    histories = []
    run = True
    while run:
        print("Press Ctrl+C to stop recording")
        try:
            ram_usage = dict(psutil.virtual_memory()._asdict())['used']/1024**3
            histories.append(ram_usage)
            time.sleep(5)
        # do
        except KeyboardInterrupt:
            break

    plt.plot(range(0,5*len(histories),5), histories)
    plt.xlabel('Time (sec)')
    plt.ylabel('Memory usage (gb)')
    plt.show()

if __name__== '__main__':
    main()
