import pickle
import glob
import sortednp as sns
import numpy as np
from itertools import combinations
import math
from datetime import datetime
from tqdm import tqdm
from multiprocessing import Pool
from functools import partial
import time


def nCr(n, r):
    f = math.factorial
    return f(n) / f(r) / f(n-r)
# base_hashtable = input("Enter base hashtables: ")
# shrink_hashtable = input("Enter shrink hashtables: ")


def find_best_base_table():
    return "./hashtables/hashtables5.pkl"
    best_table = ''
    smallest_size = 1e9
    print("Finding best base hashtable...")
    hashtable_path = glob.glob('./hashtables/*.pkl')
    for hashtable in tqdm(hashtable_path):
        with open(hashtable, 'rb') as f:
            table = pickle.load(f)
            length = len(table)
        if length < smallest_size:
            best_table = hashtable
            smallest_size = length
        del table
    print('Best table', best_table, "Num bucket", smallest_size)
    return best_table


def get_most_overlap_buckets(target, candidates):
    res = [sns.intersect(target,
                         candidate) for candidate in candidates]
    return res


def get_number_comparisions(target, useful_candidates):
    res = 0
    target_size = len(target)
    candidate_sizes = [len(i) for i in useful_candidates]
    for x, y in combinations(candidate_sizes, 2):
        res += x*y
    if target_size > sum(candidate_sizes):
        res += (target_size - sum(candidate_sizes)) * sum(candidate_sizes)
    return res


def main():
    base_name = find_best_base_table()
    candidate_names = glob.glob("./hashtables/*.pkl")
    candidate_names.remove(base_name)
    with open(base_name, 'rb') as f:
        base_hashtable = pickle.load(f)
        base_buckets = [np.array(list(x)) for x in base_hashtable.values()]
        del base_hashtable
    for candidate_name in candidate_names:
        print('START SHRINKING', candidate_name)
        with open(candidate_name, 'rb') as f:
            candidate_table = pickle.load(f)
        total_shrinked_comparisions = 0
        total_worst_comparisions = 0
        for candidate_bucket in candidate_table.values():
            candidate_bucket = np.array(list(candidate_bucket))
            start_bucket = time.time()
            useful_buckets = get_most_overlap_buckets(
                candidate_bucket, base_buckets)
            end_bucket = time.time()
            shrinked_comparisions = get_number_comparisions(
                candidate_bucket, useful_buckets)
            n = len(candidate_bucket)
            worst_comparisions = n*(n-1)/2
            print("original num comparisions", worst_comparisions)
            print("shrinked num comparisions", shrinked_comparisions)
            print("running time", end_bucket - start_bucket)
            print("time saved", (worst_comparisions - shrinked_comparisions)*2e-5)
            time.sleep(1)
            total_shrinked_comparisions += shrinked_comparisions
            total_worst_comparisions += worst_comparisions
        print("Total worst comparisions", total_worst_comparisions)
        print('Total shrinked comparisions', total_shrinked_comparisions)


if __name__ == "__main__":
    main()
