import os, sys

from neo4j.v1 import GraphDatabase, basic_auth

sys.path.insert(0,os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))))

class GetNeo4j:
    # driver = GraphDatabase.driver('bolt://124.158.1.123:7000', auth=basic_auth("neo4j", "datalake"))
    driver = GraphDatabase.driver('bolt://127.0.0.1:7687', auth=basic_auth("neo4j", "lsh"))

    def __init__(self):
        self.db = self.get_db()

    def close(self):
        self.driver.close()

    def get_db(self):
        return self.driver.session()

    @staticmethod
    def add_updated_timestamp(symbol):
        return " " + symbol + ".updated_at = localdatetime({timezone: 'Asia/Ho_Chi_Minh'}) "

    def create_node(self, key, hashtable, val, bef_key, bef_hashtable):
        # print(key, hashtable, val)
        with self.driver.session() as session:
            return session.run("MERGE (b:Bucket {key: {key}, hashtable: {hashtable}}) "
                               "ON CREATE SET b.val = ["+val+"] ON MATCH SET b.val = b.val + "+val+" "
                                "with b match (b2:Bucket {key: {bef_key}, hashtable: {bef_hashtable}}) "
                                "merge (b)-[r:Connect]-(b2) return b",
                               {"key": key, "hashtable": hashtable, "val": val,
                                "bef_key": bef_key, "bef_hashtable": bef_hashtable})

    def create_relationship_hashtable(self, id, bef_id):
        with self.driver.session() as session:
            return session.run("MATCH (b:Bucket), (b2:Bucket) where id(b) = {id} and id(b2) = {bef_id}"
                               "MERGE (b)-[r:Connect]-(b2) return b",
                               {"id": id, "bef_id": bef_id})

    def create_node_query(self, query):
        with self.driver.session() as session:
            return session.run(query)